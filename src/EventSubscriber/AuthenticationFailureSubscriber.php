<?php

namespace App\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;

use Symfony\Component\Security\Core\AuthenticationEvents;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;

use App\Service\Cacher;

/**
* This class tracks failed login attempts, and bans an ip for 1 hour after 5 attempts.
*/
class AuthenticationFailureSubscriber implements EventSubscriberInterface 
{
  
  /**
  * Login attempts before an ip is banned from trying again.
  */
  public const LIMIT = 5;
  
  /**
  * Time in seconds until login ban lifts.
  */
  public const TTL = 3600;
  
  private $request;
  private $cache;
  private $router;
  private $flash;
  
  public function __construct(RequestStack $request, Cacher $cache, RouterInterface $router, FlashBagInterface $flash) 
  {
    $this->request = $request->getCurrentRequest();
    $this->cache = $cache;
    $this->router = $router;
    $this->flash = $flash;
  }
  
  public static function getSubscribedEvents(): array 
  {
    return [ 
      AuthenticationEvents::AUTHENTICATION_FAILURE => 'onAuthenticationFailure', 
      KernelEvents::REQUEST => 'onAuthentication' 
    ];
  }
  
  public function onAuthenticationFailure() 
  {
    $ip = $this->request->getClientIp();
    if (!$ip) {
      return; }
    if ($this->cache->check($ip)) {
      $this->cache->set($ip, $this->cache->get($ip) + 1, self::TTL); 
    } else {
      $this->cache->set($ip, 1, self::TTL); }
  }
  
  public function onAuthentication(GetResponseEvent $event) 
  {
    $request = $event->getRequest();
    $route = $request->get('_route');
    if ('Login' !== $route) {
      return;}
    $ip = $request->getClientIp();
    if ($this->cache->check($ip)) {
      if ($this->cache->get($ip) >= self::LIMIT) {
        $response = new RedirectResponse($this->router->generate('Home'));
        $this->flash->add('notice', 'You have tried to login too many times. Try again in one hour.');
        $event->setResponse($response); } }
  }
}