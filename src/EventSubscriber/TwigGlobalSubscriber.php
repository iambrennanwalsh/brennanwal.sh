<?php

namespace App\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Twig\Environment;
use App\Repository\ArticleRepository;
use App\Repository\CategoryRepository;
use App\Service\Cacher;
use App\Service\Curl;

class TwigGlobalSubscriber implements EventSubscriberInterface {
	
  private $twig;
  private $params;
  private $cache;
  private $curl;
  private $featured;
  private $categories;
    
  public function __construct(Environment $twig, ParameterBagInterface $params, Cacher $cache, Curl $curl, ArticleRepository $featured, CategoryRepository $categories) {
    $this->twig = $twig;
    $this->params = $params; 
    $this->cache = $cache;
    $this->curl = $curl;
    $this->featured = $featured;
    $this->categories = $categories;
  }
  
  public static function getSubscribedEvents() {	
    return [ KernelEvents::REQUEST => [['instagramFeed'], ['featuredPosts'], ['articleCategories']] ];
  }

  public function instagramFeed() {			
    if (!$this->cache->check('instagram.feed')) {
      $temp = array();
      $token = $this->params->get('instagram_access_token');
      $api = "https://api.instagram.com/v1/users/self/media/recent/?count=10&access_token=$token";
      foreach($this->curl->get($api)->data as $item){
        $temp[] = $item->images->standard_resolution->url; }
        $this->cache->set('instagram.feed', $temp, 86400); } 
    $feed = $this->cache->get('instagram.feed');
    $this->twig->addGlobal('feed', $feed);
  }	
  
  public function featuredPosts() {  
    if (!$this->cache->check('featured.posts')) {
      $posts = $this->featured->findFeatured();
      $this->cache->set('featured.posts', $posts, 86400); } 
    $posts = $this->cache->get('featured.posts');
    $this->twig->addGlobal('featured', $posts);
  }
  
  public function articleCategories() {
    if ($this->cache->check('article.categories') == null) {
      $categories = $this->categories->findAll();
      $this->cache->set('article.categories', $categories, 86400); } 
    $categories = $this->cache->get('article.categories');
    $this->twig->addGlobal('categories', $categories);
  }
}
