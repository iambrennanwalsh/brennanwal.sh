<?php

namespace App\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use App\Event\SubmissionEvent;
use App\Entity\Submission;
use App\Service\Mailer;

class SubmissionSubscriber implements EventSubscriberInterface {
  
  private $mailer;
  
  public function __construct(Mailer $mailer) {
    $this->mailer = $mailer;
  }
    
  
  public static function getSubscribedEvents(): array {
    return [ SubmissionEvent::class => 'onSubmission'];
  }

    
  public function onSubmission(SubmissionEvent $event) {
    $submission = $event->getSubmission();
    $template = 'submission';
    $subject = $submission->getSubject();
    $content = [
      'name' => $submission->getName(),
      'email' => $submission->getEmail(),
      'message' => $submission->getMessage()];
    $this->mailer->send($template, $subject, $content); 
  }

}
