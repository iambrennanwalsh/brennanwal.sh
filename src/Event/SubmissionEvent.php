<?php

namespace App\Event;

//use Symfony\Component\EventDispatcher\Event;
use Symfony\Contracts\EventDispatcher\Event;
use App\Entity\Submission;

class SubmissionEvent extends Event {
    
  private $submission;
  
  public function __construct(Submission $submission) {
    $this->submission = $submission;
  }
  
  public function getSubmission(): Submission {
    return $this->submission;
  }
}