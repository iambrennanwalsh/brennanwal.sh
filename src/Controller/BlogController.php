<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Article;
use App\Entity\Category;
use App\Entity\Tag;

class BlogController extends AbstractController {
	
    /**
    * @Route("/blog/{page<\d+>?1}", name="Blog")
    */
	public function blog($page = 1) {
      $repository = $this->getDoctrine()->getRepository(Article::class);
      $posts = $repository->getPaginatedPosts($page);
      return $this->render('frontend/blog.twig', [
        'postsPerPage' => Article::ARTICLES_PER_PAGE,
        'totalPosts' => $posts->count(),
        'currentPage' => $page,
        'lastPage' => ceil($posts->count() / Article::ARTICLES_PER_PAGE),
        'active' => 'all',
        'articles' => $posts
      ]); }
	
    /**
    * @Route("/article/{slug}", name="Article")
    */
	public function article($slug) {
        $repository = $this->getDoctrine()->getRepository(Article::class);
		$article = $repository->findOneBy(['slug' => $slug]);
        $related = $repository->findRelated($article);
		return $this->render('frontend/article.twig', [
			'article' => $article,
            'related' => $related
		]); }
  
    /**
    * @Route("/category/{category}/{page<\d+>?1}", name="Category")
    */
	public function category($category, $page = 1) {
      $articles = $this->getDoctrine()->getRepository(Article::class);
      $categories = $this->getDoctrine()->getRepository(Category::class);
      $posts = $articles->getPaginatedPostsFromCategory($page, $category);
      $cat = $categories->findOneBy(array('slug' => $category))->getName();
      return $this->render('frontend/blog.twig', [
        'postsPerPage' => Article::ARTICLES_PER_PAGE,
        'totalPosts' => $posts->count(),
        'currentPage' => $page,
        'lastPage' => ceil($posts->count() / Article::ARTICLES_PER_PAGE),
        'active' => $category,
        'articles' => $posts,
        'pageTitle' => $cat,
        'type' => "category"
      ]); }
  
    /**
    * @Route("/tag/{tag}/{page<\d+>?1}", name="Tag")
    */
	public function tag($tag, $page = 1) {
      $articles = $this->getDoctrine()->getRepository(Article::class);
      $tags = $this->getDoctrine()->getRepository(Tag::class);
      $posts = $articles->getPaginatedPostsFromTag($page, $tag);
      $tagName = $tags->findOneBy(array('slug' => $tag))->getName();
      return $this->render('frontend/blog.twig', [
        'postsPerPage' => Article::ARTICLES_PER_PAGE,
        'totalPosts' => $posts->count(),
        'currentPage' => $page,
        'lastPage' => ceil($posts->count() / Article::ARTICLES_PER_PAGE),
        'active' => $tag,
        'articles' => $posts,
        'pageTitle' => $tagName,
        'type' => "tag"
      ]); }
}