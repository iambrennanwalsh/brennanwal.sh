<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use App\Event\SubmissionEvent;
use App\Entity\Submission;
use App\Entity\Article;
use App\Entity\Project;
  
class AdminController extends AbstractController {
  
  /**
   * @Route(path="/admin/submission/resend", name="Resend")
   */
  public function resendAction(Request $request, EventDispatcherInterface $eventDispatcher) {
    $id = $request->query->get('id');
    $em = $this->getDoctrine()->getManager();
    $submission = $em->getRepository(Submission::class)->find($id);
    $eventDispatcher->dispatch(new SubmissionEvent($submission));
    return $this->redirectToRoute('easyadmin', array('action' => 'list', 'entity' => $request->query->get('entity')));
  }
  
  /**
   * @Route(path="/admin/submission/view-article", name="ViewArticle")
   */
  public function viewArticleAction(Request $request) {
    $id = $request->query->get('id');
    $em = $this->getDoctrine()->getManager();
    $article = $em->getRepository(Article::class)->find($id);
    $route = "/article/".$article->getSlug(); 
    return $this->redirect($route);
  }
  
  /**
   * @Route(path="/admin/submission/view-project", name="ViewProject")
   */
  public function viewProjectAction(Request $request) {
    $id = $request->query->get('id');
    $em = $this->getDoctrine()->getManager();
    $project = $em->getRepository(Project::class)->find($id);
    $route = "/project/".$project->getSlug(); 
    return $this->redirect($route);
  }

}