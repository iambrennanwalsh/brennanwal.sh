<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use App\Entity\Project;
use App\Entity\Submission;
use App\Form\SubmissionType;
use App\Event\SubmissionEvent;
use App\Service\Serialize;

class FrontendController extends AbstractController {
	
    /**
    * @Route("/", name="Home")
    */
	public function home() {
      return $this->render('frontend/home.twig'); }
	
    /**
    * @Route("/about", name="About")
    */
	public function about() {
      return $this->render('frontend/about.twig'); }
	
    /**
    * @Route("/portfolio", name="Portfolio")
    */
	public function portfolio(Serialize $serialize) {
      $projects = $serialize->json($this->getDoctrine()->getRepository(Project::class)->findAll());
      return $this->render('frontend/portfolio.twig', [
        'projects' => $projects
      ]); 
    }
	
    /**
    * @Route("/project/{slug}", name="Project")
    */
	public function project($slug) {
      $repository = $this->getDoctrine()->getRepository(Project::class);
      $project = $repository->findOneBy(['slug' => $slug]);
      return $this->render('frontend/project.twig', [
		'project' => $project
      ]); }
	
    /**
    * @Route("/contact", name="Contact")
    */
	public function contact(Request $request, EventDispatcherInterface $eventDispatcher) {
      $submission = new Submission();
      $form = $this->createForm(SubmissionType::class, $submission);
      $form->handleRequest($request);
      if($form->isSubmitted() && $form->isValid()) {
        $entityManager = $this->getDoctrine()->getManager();
		$entityManager->persist($submission);
		$entityManager->flush();
        $eventDispatcher->dispatch(new SubmissionEvent($submission)); }
      return $this->render('frontend/contact.twig', [
        'form' => $form->createView()
      ]); }
}