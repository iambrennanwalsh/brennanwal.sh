<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;	
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class AuthenticationController extends AbstractController {
	
    /**
    * @Route("/login", name="Login")
    */
	public function login(Request $request, AuthenticationUtils $authenticationUtils) {
		$error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();
		
		return $this->render('authentication/login.twig', array(
        'last_username' => $lastUsername,
        'error'         => $error,
    )); }
    
    
    /**
    * @Route("/logout", name="Logout")
    */
	public function logout() {}
}