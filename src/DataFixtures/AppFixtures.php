<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\File\File;
use Faker\Generator;
use App\Service\Slugger;
use App\Service\Root;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Entity\Category;
use App\Entity\Tag;
use App\Entity\ProjectCategory;
use App\Entity\User;
use App\Entity\Article;
use App\Entity\Project;
use App\Entity\Comments;
use App\Entity\ImageFolder;
use App\Entity\Image;

class AppFixtures extends Fixture {
  
  private $encoder;
  private $slugger;
  private $faker;
  private $root;
  
  public function __construct(Root $root, Slugger $slugger, UserPasswordEncoderInterface $encoder) {
    $this->root = $root->getRootPath();
    $this->encoder = $encoder;
    $this->slugger = $slugger; 
    $this->faker = Factory::create();}
  
  public function load(ObjectManager $manager): void {
    
    // Images
    $this->loadImageFolders($manager);
    $this->loadImages($manager);

    // Categories
    $this->loadCategories($manager);
    $this->loadTags($manager);
    $this->loadProjectCategories($manager);
    
    // User
    $this->loadUsers($manager);
    
    // Articles & Projects
    $this->loadArticles($manager);
    $this->loadProjects($manager);
    
    // Comments
    #$this->loadComments($manager);
    
    $manager->flush();
  }
  
  public function loadImageFolders($manager) {
    $imageFolderData = ['Interface', 'Me', 'Skills', 'Slides', 'Portfolio', 'Articles'];
    for ($i = 0; $i < count($imageFolderData); $i++) {
      $imageFolder = new ImageFolder();
      $imageFolder->setName($imageFolderData[$i]);
      $imageFolder->setPath($this->slugger->slugify($imageFolderData[$i]));
      $manager->persist($imageFolder);
      $this->addReference("imagefolder-$imageFolderData[$i]" , $imageFolder);
    }
  }
  
  public function loadImages($manager) {
    $folders = ['Interface', 'Me', 'Skills', 'Slides', 'Portfolio', 'Articles'];
    $root = $this->root . "/public/images/";
    foreach($folders as $folder) {
      $slug = $this->slugger->slugify($folder);
      $finder = new Finder();
      $finder->files()->in($root . $slug);
      if ($folder == 'Articles' || $folder == 'Portfolio') {
          $i = 0; }
      foreach( $finder as $unit ){
        $image = new Image();
        $file = new File($unit->getRealPath());
        $image->setFilename($file->getFilename());
        $image->setFile($file);
        $image->setAlt("");
        $image->setFolder($this->getReference("imagefolder-$folder"));
        $manager->persist($image);
        if ($folder == 'Articles' || $folder == 'Portfolio') {
          $i++; 
          $this->addReference("image-$slug-$i", $image);
        }
      }
    }
  }
  
  public function loadCategories($manager) {
    $categoryData = ['Php', 'Doctrine', 'JavaScript', 'Webpack', 'Testing', 'Ruby', 'Symfony', 'Laravel', 'Swift 5'];
    for($i = 0; $i < count($categoryData); $i++) {
      $category = new Category();
      $category->setName($categoryData[$i]);
      $category->setSlug($this->slugger->slugify($categoryData[$i]));
      $manager->persist($category);
      $this->addReference("category-$i" , $category);}
  }
  
  public function loadTags($manager) {
    $tagData = ['Php', 'Doctrine', 'JavaScript', 'Webpack', 'Testing', 'Ruby', 'Symfony', 'Laravel', 'Swift 5'];
    for($i = 0; $i < count($tagData); $i++) {
      $tag = new Tag();
      $tag->setName($tagData[$i]);
      $tag->setSlug($this->slugger->slugify($tagData[$i]));
      $manager->persist($tag);
      $this->addReference("tag-$i" , $tag);}
  }
  
  public function loadProjectCategories($manager) {
    $projectCategoryData = ['Past Projects', 'Current Projects'];
    for($i = 0; $i < count($projectCategoryData); $i++) {
      $projectCategory = new ProjectCategory();
      $projectCategory->setName($projectCategoryData[$i]);
      $manager->persist($projectCategory);
      $this->addReference("project-category-$i" , $projectCategory);}
  }
  
  public function loadUsers($manager) {
    $user = new User();
    $user->setName('Admin');
    $user->setEmail('admin@brennanwal.sh');
    $user->setUsername('admin');
    $user->setRoles(["ROLE_ADMIN"]);
    $user->setSummary('I am admin. God of this domain. You will bow to me.');
    $user->setPassword("admin");
    $manager->persist($user);
    $this->addReference("admin" , $user);
  }
  
  public function loadArticles($manager) {
    for($i = 0; $i < 50; $i++) {
       $article = new Article();
       $title = $this->faker->unique()->realText($maxNbChars = 30);
       $article->setTitle($title);
       $article->setSlug($this->slugger->slugify($title));
       $article->setSummary($this->faker->realText($maxNbChars = 100));
       $html = "<div class='box is-clearfix'><h2>".$this->faker->realText($maxNbChars = 30)."</h2><div class='content'><div class='unit no-heading'><p>".$this->faker->text($maxNbChars = 1000)."</p> <hr></div></div></div><div class='box is-clearfix'><h2>".$this->faker->realText($maxNbChars = 30)."</h2><div class='content'><div class='unit'><h3>".$this->faker->realText($maxNbChars = 30)."</h3><p>".$this->faker->text($maxNbChars = 1000)."</p> <hr></div><div class='unit'><h3>".$this->faker->realText($maxNbChars = 30)."</h3> <p>".$this->faker->text($maxNbChars = 1000)."</p> <hr></div></div></div>";
       $article->setContent($html);
       $article->setPublishedAt(new \DateTime());
       $article->setFeatured($this->faker->boolean(10));
       $article->setCategory($this->getReference("category-".rand(0, 8)));
       $article->setImage($this->getReference("image-articles-". rand(1, 20)));
       $article->addTag($this->getReference("tag-".rand(0, 8)));
       if (rand(0,2) == 2) {
         $article->addTag($this->getReference("tag-".rand(0, 8))); }
       $article->setAuthor($this->getReference("admin"));
       $manager->persist($article);}
  }
  
  public function loadProjects($manager) {
    for($i = 0; $i < 9; $i++) {
       $project = new Project();
       $title = $this->faker->unique()->realText($maxNbChars = 30);
       $project->setName($title);
       $project->setSlug($this->slugger->slugify($title));
       $project->setImage($this->getReference("image-portfolio-". rand(1, 20)));
       $project->setSummary($this->faker->realText($maxNbChars = 100));
       $html = "<div class='box is-clearfix'><div class='content'><div class='unit'><img src='' class='lightbox'> <h3>".$this->faker->realText($maxNbChars = 30)."</h3> <p>".$this->faker->text($maxNbChars = 1000)."</p> <hr></div><div class='unit'><h3>".$this->faker->realText($maxNbChars = 30)."</h3> <p>".$this->faker->text($maxNbChars = 1000)."</p> <hr></div></div></div>";
       $project->setContent($html);
       $rand = rand(0, 1);
       $project->setCategory($this->getReference("project-category-$rand"));
       $manager->persist($project);}
  }
}
