<?php

namespace App\Repository;

use App\Entity\Article;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class ArticleRepository extends ServiceEntityRepository {
	
  public function __construct(RegistryInterface $registry) {
    parent::__construct($registry, Article::class); }
  
  
  public function findFeatured(int $amount = 4) {
    return $this->createQueryBuilder('a')
      ->andWhere('a.featured = :val')
      ->setParameter('val', true)
      ->orderBy('a.publishedAt', 'DESC')
      ->setMaxResults($amount)
      ->getQuery()
      ->getResult(); }
  
  
  public function getPaginatedPosts($page = 1) {
    $limit = Article::ARTICLES_PER_PAGE;
    $query = $this->createQueryBuilder('a')
      ->orderBy('a.publishedAt', 'DESC')
      ->getQuery();
    $paginator = new Paginator($query);
    $paginator->getQuery()
      ->setFirstResult($limit * ($page - 1))
      ->setMaxResults($limit);
    return $paginator; }
  
  
  public function getPaginatedPostsFromCategory($page = 1, $category) {
    $limit = Article::ARTICLES_PER_PAGE;
    $query = $this->createQueryBuilder('a')
        ->leftJoin('a.category', 'c')
        ->andWhere('c.slug = :val')
        ->setParameter('val', $category)
        ->orderBy('a.publishedAt', 'DESC')
        ->getQuery();
    $paginator = new Paginator($query);
    $paginator->getQuery()
      ->setFirstResult($limit * ($page - 1))
      ->setMaxResults($limit);
    return $paginator; }
  
  
  public function getPaginatedPostsFromTag($page = 1, $tag) {
    $limit = Article::ARTICLES_PER_PAGE;
    $query = $this->createQueryBuilder('a')
        ->leftJoin('a.tags', 'c')
        ->andWhere('c.slug = :val')
        ->setParameter('val', $tag)
        ->orderBy('a.publishedAt', 'DESC')
        ->getQuery();
    $paginator = new Paginator($query);
    $paginator->getQuery()
      ->setFirstResult($limit * ($page - 1))
      ->setMaxResults($limit);
    return $paginator; }
  
  public function findRelated($article) {
    $related = $this->findBy(['category' => $article->getCategory()]); 
    $arr = array();
    $i = 0;
    shuffle($related);
    foreach($related as $post) {
      if ($i == 3) {
        return $arr;}
      if($post != $article) {
        array_push($arr, $post);
        $i++;
      }
    }
    return $arr;
  }
}