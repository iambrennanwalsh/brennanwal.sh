<?php

namespace App\Doctrine;

use App\Entity\Image;
use App\Entity\ImageFolder;
use Doctrine\ORM\Events;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Event\LifecycleEventArgs;
use App\Service\FileMover;
use App\Service\Root;

class ImageFolderSubscriber implements EventSubscriber {
  
  private $filesystem;
  private $root;
  
  public function __construct(Root $root, FileMover $filesystem) {
    $this->filesystem = $filesystem;
    $this->root = $root->getRootPath() . "/public/images/";
  }
	
  public function getSubscribedEvents() {
    return array(Events::preUpdate, Events::prePersist, Events::preRemove); }
	
  
  public function preUpdate(PreUpdateEventArgs $args) {
    $entity = $args->getEntity();
    if ($entity instanceof ImageFolder && $args->hasChangedField('path')) {
      $from = $this->root . $args->getOldValue('path');
      $to = $this->root . $args->getNewValue('path');
      $this->filesystem->rename($from, $to);
      $em = $args->getEntityManager()->getRepository(Image::class);
      $images = $em->findBy(array('folder' => $entity));
      foreach($images as $value) {
        $value->setPath($to);
        $value->setPublicPath("/images/" . $args->getNewValue('path'));
      }
    }
  }
  
  public function prePersist(LifecycleEventArgs $args) {
  	$entity = $args->getEntity();
    if ($entity instanceof ImageFolder) {
      $folder = $this->root . $entity->getPath();
      if (!$this->filesystem->exists([$folder])) {
         $this->filesystem->mkdir($folder);
      }
    } 
  }
  
  public function preRemove(LifecycleEventArgs $args) {
    $entity = $args->getEntity();
    if ($entity instanceof ImageFolder) {
      $folder = $this->root . $entity->getPath();
      if (!$this->filesystem->exists([$folder])) {
         $this->filesystem->remove([$folder]);
      }
    }
  }
} 
