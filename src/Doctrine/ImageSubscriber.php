<?php

namespace App\Doctrine;

use App\Entity\Image;
use Doctrine\ORM\Events;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Event\LifecycleEventArgs;
use App\Service\Uploader;
use App\Service\FileMover;
use App\Service\Root;

class ImageSubscriber implements EventSubscriber {
  
  private $uploader;
  private $filesystem;
  private $root;
  
  public function __construct(Root $root, Uploader $uploader, FileMover $filesystem) {
    $this->uploader = $uploader; 
    $this->filesystem = $filesystem;
    $this->root = $root->getRootPath() . '/public';
  }
	
  public function getSubscribedEvents() {
    return array(Events::preUpdate, Events::prePersist, Events::preRemove); }
	
  public function preUpdate(PreUpdateEventArgs $args) {
  	$entity = $args->getEntity();
    if ($entity instanceof Image) {
      if ($args->hasChangedField('filename')) {
        $path = $entity->getPath();
        $from = $path . $args->getOldValue('filename');
        $to = $path . $args->getNewValue('filename');
        $this->filesystem->rename($from, $to);
      }
      if ($args->hasChangedField('folder')) {
        $newFolder = $entity->getFolder();
        $from = $entity->getPath() . $entity->getFilename();
        $to = $this->root . $entity::DEFAULT_PATH;
        $public = $entity::DEFAULT_PATH;
        if ($newFolder) {
          $to .= $newFolder->getPath(); 
          $public .= $newFolder->getPath();}
        $this->filesystem->copy($from, $to . $entity->getFilename());
        $this->filesystem->remove([$from]); 
        $entity->setPath($to);
        $entity->setPublicPath($public . $entity->getFilename());
      }
    }
  }
  
  public function prePersist(LifecycleEventArgs $args) {
  	$entity = $args->getEntity();
    if ($entity instanceof Image) {
      $filename = $entity->getFilename();
      $file = $entity->getFile();
      $folder = $entity->getFolder();
      $path = $this->root . $entity::DEFAULT_PATH;
      $public = $entity::DEFAULT_PATH;
      if ($folder) {
        $path .= $folder->getPath(); 
        $public .= $folder->getPath();}
      $this->uploader->upload($file, $path, $filename);
      $entity->setPath($path);
      $entity->setPublicPath($public . $entity->getFilename());
    } 
  }
  
  public function preRemove(LifecycleEventArgs $args) {
    $entity = $args->getEntity();
    if ($entity instanceof Image) {
      $path = $entity->getPath();
      $filename = $entity->getFilename();
      $this->filesystem->remove([$path . $filename]);
    }
  }
} 
