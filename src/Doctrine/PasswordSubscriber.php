<?php

namespace App\Doctrine;

use App\Entity\User;
use Doctrine\ORM\Events;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Event\LifecycleEventArgs;
use App\Service\Encoder;

class PasswordSubscriber implements EventSubscriber {
	
  private $encoder; 
	
  public function __construct(Encoder $encoder) {	
    $this->encoder = $encoder; }
	
  public function getSubscribedEvents() {
    return array(Events::preUpdate, Events::prePersist); }
	
  public function preUpdate(PreUpdateEventArgs $args) {
  	$entity = $args->getEntity();
    if ($entity instanceof User && $args->hasChangedField('password')) {
      if ($args->getNewValue('password') == "") {
        $args->setNewValue('password', $args->getOldValue('password'));
        return;}
      $args->setNewValue('password', $this->encoder->encode($entity, $args->getNewValue('password'))); } }
  
  public function prePersist(LifecycleEventArgs $args) {
  	$entity = $args->getEntity();
    if ($entity instanceof User) {
		$entity->setPassword($this->encoder->encode($entity, $entity->getPassword())); } }
} 
