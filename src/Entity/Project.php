<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
	
/**
 * A project.
 * @ORM\Entity()
 * @ORM\Table(name="public.project")
 */
	
class Project {
	
    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $name;
	
    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $summary;
	
    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $slug;

    /**
     * @var string
     * @ORM\Column(type="text")
     */
    private $content;
    
    /**
     * @var ProjectCategory
     * @ORM\ManyToOne(targetEntity="ProjectCategory", fetch="EAGER")
     * @ORM\JoinColumn(nullable=false)
     */
    private $category;
    
    /**
    * @var Image
    * @ORM\ManyToOne(targetEntity="Image", fetch="EAGER")
    * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
    */ 
    private $image;

			
    public function getId(): ?int {
        return $this->id; }

	
    public function getName(): ?string {
        return $this->name; }

    public function setName(string $name): self {
        $this->name = $name;
        return $this; }
  
    
    public function getSummary(): ?string {
        return $this->summary; }

    public function setSummary(string $summary): self {
        $this->summary = $summary;
        return $this; }

	 
    public function getSlug(): ?string {
        return $this->slug; }
	
    public function setSlug(string $slug): self {
        $this->slug = $slug;
        return $this; }


    public function getContent(): ?string {
        return $this->content; }

    public function setContent(string $content): self {
        $this->content = $content;
        return $this; }
  
  
    public function getCategory(): ?ProjectCategory {
        return $this->category; }
  
    public function setCategory(ProjectCategory $category): void {
        $this->category = $category; }
  
  
    public function getImage(): ?Image {
        return $this->image; }
  
    public function setImage(Image $image): void {
        $this->image = $image; }
  
}
