<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * An article.
 * @ORM\Entity(repositoryClass="App\Repository\ArticleRepository")
 * @ORM\Table(name="public.article")
 */
class Article {
  
    public const ARTICLES_PER_PAGE = 9;
  
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
  
    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $title;
  
    /**
     * @var string
     * @ORM\Column(type="string", unique=true)
     */
    private $slug;
  
    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $summary;
  
    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    private $content;
  
    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    private $featured;
  
    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $publishedAt;
  
    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="User", fetch="EAGER")
     */
    private $author;
    
    /**
     * @var Category
     * @ORM\ManyToOne(targetEntity="Category", fetch="EAGER")
     * @ORM\JoinColumn(name="category", referencedColumnName="id")
     */
    private $category;
  
    /**
     * @var Tag[]|ArrayCollection
     * @ORM\ManyToMany(targetEntity="Tag", cascade={"persist"}, fetch="EAGER")
     * @ORM\OrderBy({"name": "ASC"})
     */
    private $tags;
  
    /**
     * @var Comment[]|ArrayCollection
     * @ORM\OneToMany(targetEntity="Comment", mappedBy="article", orphanRemoval=true, cascade={"persist"})
     * @ORM\OrderBy({"publishedAt": "DESC"})
     */
    private $comments;
  
    /**
    * @var Image
    * @ORM\ManyToOne(targetEntity="Image", fetch="EAGER")
    * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
    */ 
    private $image;
  
  
    public function __construct() {
        $this->publishedAt = new \DateTime();
        $this->comments = new ArrayCollection();
        $this->tags = new ArrayCollection(); }
  
  
    public function getId(): ?int {
        return $this->id; }
  
  
    public function getTitle(): ?string {
        return $this->title;}
    
    public function setTitle(string $title): void {
        $this->title = $title; }
  
  
    public function getSlug(): ?string {
        return $this->slug; }
  
    public function setSlug(string $slug): void {
        $this->slug = $slug; }
  
    
    public function getSummary(): ?string {
        return $this->summary;}
    
    public function setSummary(string $summary): void {
        $this->summary = $summary; }
  
  
    public function getContent(): ?string {
        return $this->content;}
  
    public function setContent(string $content): void {
        $this->content = $content; }
  
  
    public function getFeatured(): ?bool {
        return $this->featured; }
  
    public function setFeatured(bool $featured): void {
        $this->featured = $featured; }
  
  
    public function getPublishedAt(): \DateTime {
        return $this->publishedAt; }
  
    public function setPublishedAt(\DateTime $publishedAt): void {
        $this->publishedAt = $publishedAt; }
  
  
    public function getAuthor(): ?User {
        return $this->author; }
  
    public function setAuthor(User $author): void {
        $this->author = $author; }
  
  
    public function getCategory(): ?Category {
        return $this->category; }
  
    public function setCategory(Category $category): void {
        $this->category = $category; }  
  
  
    public function addTag(Tag ...$tags): void {
        foreach ($tags as $tag) {
            if (!$this->tags->contains($tag)) {
                $this->tags->add($tag); }}}
  
    public function removeTag(Tag $tag): void {
        $this->tags->removeElement($tag); }
  
    public function getTags(): Collection {
        return $this->tags; }
  
  
    public function getComments(): Collection {
        return $this->comments; }
  
    public function addComment(Comment $comment): void {
        $comment->setPost($this);
        if (!$this->comments->contains($comment)) {
            $this->comments->add($comment); } }
  
    public function removeComment(Comment $comment): void {
        $this->comments->removeElement($comment); }
  
    
    public function getImage(): ?Image {
        return $this->image; }
  
    public function setImage(Image $image): void {
        $this->image = $image; }

}