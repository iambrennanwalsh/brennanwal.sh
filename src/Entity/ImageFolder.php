<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * An image folder.
 * @ORM\Entity()
 * @ORM\Table(name="public.image_folder")
 */
class ImageFolder {
    
  /**
  * @var int
  * @ORM\Id
  * @ORM\GeneratedValue
  * @ORM\Column(type="integer")
  */
  private $id;
  
  /**
  * The display name of this folder.
  * @var string
  * @ORM\Column(type="string")
  */
  private $name;
  
  /**
  * The path to upload images to.
  * @var string
  * @ORM\Column(type="string", unique=true)
  */
  private $path;
  
  /**
  * @var Image[]|ArrayCollection
  * @ORM\OneToMany(targetEntity="Image", mappedBy="folder", cascade={"remove"}))
  * @ORM\JoinColumn(nullable=true)
  */
  private $images;
  
  public function __construct() {
    $this->images = new ArrayCollection();
  }
  
  public function getId(): ?int {
    return $this->id; }
  
  
  public function getName(): ?string {
    return $this->name;}
    
  public function setName(string $name): void {
    $this->name = $name; }
  
  
  public function getPath(): ?string {
    return $this->path; }
  
  public function setPath(string $path): void {
    if (substr($path, -1) != "/") {
      $path .= "/";
    }
    $this->path = $path; }
  
  
  public function getImages(): Collection {
    return $this->images; }
  
  public function addImage(Image $image): void {
    $image->setFolder($this);
    if (!$this->images->contains($image)) {
      $this->images->add($image); } }
  
  public function removeImage(Image $image): void {
    $this->images->removeElement($image); }
  
  public function __toString() {
    return $this->getName(); }

}