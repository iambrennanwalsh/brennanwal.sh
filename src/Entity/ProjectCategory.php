<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * A project category.
 * @ORM\Entity()
 * @ORM\Table(name="public.project_category")
 */
class ProjectCategory {
  
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
  
    /**
     * @var string
     * @ORM\Column(type="string", unique=true)
     */
    private $name;
  
    public function getId(): ?int {
        return $this->id; }
  
  
    public function setName(string $name): void {
        $this->name = $name; }
  
    public function getName(): ?string {
        return $this->name; }
  
    public function __tostring() {
      return $this->getName();
    }
}