<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * An image.
 * @ORM\Entity()
 * @ORM\Table(name="public.image")
 */
class Image {
  
  const DEFAULT_PATH = "/images/";
    
  /**
  * @var int
  * @ORM\Id
  * @ORM\GeneratedValue
  * @ORM\Column(type="integer")
  */
  private $id;
  
  /** 
  * Unmapped property that temporatily holds the file for upload.
  * @var UploadedFile
  */
  private $file;

  /**
  * The images filename.
  * @var string
  * @ORM\Column(type="string", length=255)
  */
  private $filename;
  
  /**
  * The path to the image.
  * @var string
  * @ORM\Column(type="string", length=255)
  */
  private $path;
  
  /**
  * The web accessible path.
  * @var string
  * @ORM\Column(type="string", length=255)
  */
  private $publicPath;
  
  /**
  * @var string
  * @ORM\Column(type="string", nullable=true)
  */
  private $alt;
  
  /**
  * @var ImageFolder
  * @ORM\ManyToOne(targetEntity="ImageFolder", inversedBy="images")
  * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
  */
  private $folder;
  
  
  public function getId(): ?int {
    return $this->id; }
  
  public function setFile($file = null): void {
    $this->file = $file;}

  public function getFile() {
    return $this->file; }

  public function setFilename(?string $filename): void {
    $this->filename = $filename;}

  public function getFilename(): ?string {
    return $this->filename; }
  
  public function setPath(?string $path): void {    
    $this->path = $path;}
  
  public function getPath(): ?string {
    return $this->path;}
  
  public function setPublicPath(?string $path): void {    
    $this->publicPath = $path;}
  
  public function getPublicPath(): ?string {
    return $this->publicPath;}
  
  public function setAlt(string $alt): void {
    $this->alt = $alt; }
  
  public function getAlt(): ?string {
    return $this->alt; }
  
  public function getFolder(): ?ImageFolder {
    return $this->folder; }
  
  public function setFolder(?ImageFolder $folder): void {
    $this->folder = $folder; }
  
  public function __tostring() {
    return $this->getPublicPath(); }
  
  /**
  * For EasyAdmin to display image on list pages.
  */
  public function getImage() {
    return $this->publicPath;}

}