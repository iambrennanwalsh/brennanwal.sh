<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * An article tag.
 * @ORM\Entity()
 * @ORM\Table(name="public.tag")
 */
class Tag {
  
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
  
    /**
     * @var string
     * @ORM\Column(type="string", unique=true)
     */
    private $name;
  
    /**
     * @var string
     * @ORM\Column(type="string", unique=true)
     */
    private $slug;
  
  
    public function getId(): ?int {
        return $this->id; }
  
  
    public function setName(string $name): void {
        $this->name = $name; }
  
    public function getName(): ?string {
        return $this->name; }
    
  
    public function setSlug(string $slug): void {
        $this->slug = $slug; }
  
    public function getSlug(): ?string {
        return $this->slug; }
  
  
    public function __tostring() {
      return $this->getName();
    }
}