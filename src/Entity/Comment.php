<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * A comment on an article.
 * @ORM\Entity()
 * @ORM\Table(name="public.comment")
 */
class Comment {
  
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
  
    /**
     * @var string
     * @ORM\Column(type="text")
     */
    private $content;
  
    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $publishedAt;
  
    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="User")
     */
    private $author;
  
    /**
     * @var Article
     * @ORM\ManyToOne(targetEntity="Article", inversedBy="comments")
     */
    private $article;
  
  
    public function __construct() {
        $this->publishedAt = new \DateTime(); }
  
  
    public function getId(): ?int {
        return $this->id; }
  
  
    public function getContent(): ?string {
        return $this->content; }
  
    public function setContent(string $content): void {
        $this->content = $content; }
  
  
    public function getPublishedAt(): \DateTime {
        return $this->publishedAt; }
  
    public function setPublishedAt(\DateTime $publishedAt): void {
        $this->publishedAt = $publishedAt; }
  
  
    public function getAuthor(): ?User {
        return $this->author; }
  
    public function setAuthor(User $author): void {
        $this->author = $author; }
  
    
    public function getArticle(): ?Article {
        return $this->post; }
  
    public function setArticle(Article $article): void {
        $this->article = $article; }
  
}