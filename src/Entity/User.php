<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * A user.
 * @ORM\Entity()
 * @ORM\Table(name="public.user")
 */
class User implements UserInterface {
  
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
  
    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $name;
  
    /**
     * @var string
     * @ORM\Column(type="string", unique=true)
     */
    private $username;
  
    /**
     * @var string
     * @ORM\Column(type="string", unique=true)
     */
    private $email;
  
    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $password;
  
    /**
     * @var array
     * @ORM\Column(type="json")
     */
    private $roles = [];
    
    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $summary;
  
    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $gravatar;
    

  
    public function getId(): ?int {
        return $this->id; }
 
  
    public function getName(): ?string {
        return $this->name; }
  
    public function setName(string $name): void {
        $this->name = $name; }
 
  
    public function getUsername(): ?string {
        return $this->username; }
  
    public function setUsername(string $username): void {
        $this->username = $username; }
 
  
    public function getEmail(): ?string {
        return $this->email; }
  
    public function setEmail(string $email): void {
        $this->email = $email; 
        $this->gravatar = "https://www.gravatar.com/avatar/" . md5($email) . "?s=500"; }
 
  
    public function getPassword(): ?string {
        return $this->password; }
  
    public function setPassword($password): void {
      $this->password = $password; }
  
  
    public function getRoles(): array {
        $roles = $this->roles;
        if (empty($roles)) {
            $roles[] = 'ROLE_USER'; }
        return array_unique($roles); }
  
    public function setRoles(array $roles): void {
        $this->roles = $roles; }
 
  
    public function getSummary(): ?string {
        return $this->summary; }
  
    public function setSummary(string $summary): void {
        $this->summary = $summary; }
  
  
    public function getGravatar(): ?string {
        return $this->gravatar; }
    
  
    public function getSalt() {}
    
    public function eraseCredentials() {}
  
    public function __toString() {
      return $this->getUsername();
    }
}