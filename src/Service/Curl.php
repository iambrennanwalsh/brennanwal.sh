<?php

namespace App\Service;

class Curl {
  
  public function get($resource) {
    $curl = curl_init($resource);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    $result = curl_exec($curl);
    curl_close($curl);
    return json_decode($result); 
  }
		
}