<?php

namespace App\Service;

use JMS\Serializer\SerializerInterface;

class Serialize {
  
  private $serializer;
  
  public function __construct(SerializerInterface $serializer) {
    $this->serializer = $serializer;
  }
  
  public function json($content) {
    return $this->serializer->serialize($content, 'json');
  }
  
}