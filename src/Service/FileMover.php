<?php

namespace App\Service;

use Symfony\Component\Filesystem\Filesystem;

class FileMover {
  
  private $filesytem;
    
  public function __construct(Filesystem $filesystem) {
    $this->filesystem = $filesystem;
  }
  
  public function mkdir($folder, $permissions = 0744) {
    $this->filesystem->mkdir($folder, $permissions);
  }
  
  public function exists(array $path) {
    $this->filesystem->exists($path);
  }
  
  public function remove(array $path) {
    $this->filesystem->remove($path);
  }
  
  public function copy($file, $to) {
    $this->filesystem->copy($file, $to, true);
  }
  
  public function rename($from, $to) {
    $this->filesystem->rename($from, $to);
  }
}