<?php

namespace App\Service;

class Slugger {
  
  public function slugify(string $text): string {
    $text = preg_replace('/\W+/', '-', $text);
    $text = str_replace('_', '', $text);
    $text = strtolower($text);
    $text = trim($text, '-');
    if (empty($text)) {
      return 'na';}
    return $text;}  
}