<?php

namespace App\Service;

use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Entity\User;

class Encoder {
  
  private $encoder;
  
  public function __construct(UserPasswordEncoderInterface $encoder) {
    $this->encoder = $encoder;
  }
  
  public function encode(User $user, $password) {
    return $this->encoder->encodePassword($user, $password);
  }
  
}