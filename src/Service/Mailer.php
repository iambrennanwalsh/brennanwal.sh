<?php

namespace App\Service;

use Twig\Environment;

class Mailer {
	
	private $twig;
	private $mailer;
	
	public function __construct(Environment $twig, \Swift_Mailer $mailer) {
		$this->twig = $twig;
		$this->mailer = $mailer;
	}
	
	public function send($template, $subject, $content = [], $to = 'videncrypt@gmail.com') {
		$message = (new \Swift_Message())
		  ->setSubject($subject)
		  ->setFrom('mail@videncrypt.com')
		  ->setTo($to)
		  ->setBody( $this->twig->render('email/' . $template . '.twig', ['content' => $content] ), 'text/html');
		$this->mailer->send($message);
	}
	
}