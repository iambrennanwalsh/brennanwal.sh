<?php

namespace App\Service;

use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use App\Service\Root;

class Cacher {
	
  private $pool;
  
  public function __construct(Root $root) {
    $dir = $root->getRootPath() . "/public/cache";
    $this->pool = new FilesystemAdapter('', 0, $dir);
    $this->prune(); }
	
  public function check($key) {
    if ($this->pool->hasItem($key)) {
      return true; } }
  
  public function set($key, $content, $expiration) {
    $item = $this->pool->getItem($key);
    $item->set($content);
    $item->expiresAfter($expiration);
    $this->pool->save($item); }
  
  public function get($key) {
    if ($this->check($key)) {
      return $this->pool->getItem($key)->get(); } }
  
  public function prune() {
    $this->pool->prune(); }
  
  public function clear() {
    $this->pool->clear();}
  
  public function delete($key) {
    $this->pool->deleteItem($key); }
}
