<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\File\UploadedFile;

  class Uploader {
   
    function upload($image, string $path, string $filename = null) {
      $image->move($path, $filename);
      return $filename;
    }
    
    function getFilename($image) {
      return $image->getClientOriginalName();
    }
   
 }