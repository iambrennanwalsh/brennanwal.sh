// Vue
import Vue from 'vue';
window.Vue = Vue;

import Prism from 'prismjs';

// app.vue
require('./../vue/app.vue');

// app.scss
import './../scss/app.scss';