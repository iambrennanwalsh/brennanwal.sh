<p align="center"><img width="150px" src="https://raw.githubusercontent.com/iambrennanwalsh/brennanwal.sh/master/public/images/interface/tree.png"></p>
<p align="center"><img width="400px"  src="https://raw.githubusercontent.com/iambrennanwalsh/brennanwal.sh/master/public/images/logo.png"></p>

-------------------------------------------

## Contents
1. **Overview**
  - Features Checklist
2. **Frontend**
  - Webpack
  - Vue.js
  - Sass
3. **Backend**
  - Symfony 4
  - Doctrine ORM
  - EasyAdmin
  - Nginx
4. **Deployment** 
  - Heroku
  - Git & Github
5. **Building**
  - Cloning
  - Composer
  - Assets
  - Database & Fixtures

-------------------------------------------

## 1. Overview

### Features Checklist

- [x] @mentions, #refs, [links](), **formatting**, and <del>tags</del> supported
- [x] list syntax required (any unordered or ordered list supported)
- [x] this is a complete item
- [ ] this is an incomplete item


## 2. Frontend

### Webpack
### Vue.js
### Sass

## 3. Backend

### Symfony 4
### Doctrine ORM
### EasyAdmin
### Nginx

## 4. Deployment

### Heroku
### Git and Github

## 5. Building

### Cloning
### Composer
### Assets
### Database & Fixtures